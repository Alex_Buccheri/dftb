!**************************************************************************************************
! Test DFTB library API for DL POLY. V1. March 2018
! Alexander Buccheri. CCC UoB. ab17369@bristol.ac.uk 
!
! Read in DL CONFIG file, assign data to DFTB+ geometry object and pass to DFTB+ to do calculation
! Do 2 time steps for the same geometry - Forces output should be the same both times 
!**************************************************************************************************

program call_lib
  !Precision and types 
  use accuracy,       only: dp,mc
  use io,             only: stdOut
  use typegeometry,   only: TGeometry

  !Library and API
  use dftb_libwrapper,only: w_calculate_dftb_forces
  use dftb_api,       only: fill_DFTB_geometry,  print_geometry_data
  implicit none

  !-----------------
  !Declarations
  !-----------------
  !DL Poly CONFIG data
  integer                       :: levcfg, imcon, megatm, itr, nrite
  real(dp),         allocatable :: xxx(:),yyy(:),zzz(:),cell(:),fxx(:),fyy(:),fzz(:)
  character(len=8), allocatable :: atmnam(:)
  Character(Len=8), allocatable :: unqatm(:)

  !Modular variables/arrays/objects  
  type(TGeometry)       :: geo
  real(dp), allocatable :: forces(:,:)

  !Local variables 
  integer,  parameter   :: Nsteps=2
  
  !-----------------
  !Main Routine
  !-----------------
  !Assign DLPOLY variables (read from file)
  call read_DLPOLY_vars(levcfg, imcon, megatm, cell, xxx,yyy,zzz, atmnam)
  allocate(fxx(megatm),fyy(megatm),fzz(megatm),forces(3,megatm))
  
  !Mop up with dummy values
  nrite=1
  allocate(unqatm(1))
  unqatm(1)='dummy'

  !"MD loop"  
  do itr=1,Nsteps
     write(*,*) 'On time step ',itr
     !Move atomic positions 
     !call move_atomic_positions_rand(itr,xxx,yyy,zzz)
     !Compute forces with DFTB+
     call w_calculate_dftb_forces(itr,nrite, levcfg,imcon,megatm,cell,xxx,yyy,zzz,atmnam,unqatm,&
                                  fxx,fyy,fzz)
     !Output forces
     forces=0._dp
     forces(1,:)=fxx(:)
     forces(2,:)=fyy(:)
     forces(3,:)=fzz(:)
     call output_forces(itr, forces, 'DFTB')
  enddo
  
contains

  !**********************************************************************************
  ! When using the DFTB+ library in DLPOLY, there will be no need for this subroutine
  !**********************************************************************************
  subroutine read_DLPOLY_vars(levcfg, imcon, megatm, cell, xxx,yyy,zzz, atmnam)
    implicit none

    !-----------------
    !Declarations
    !----------------
    !Modular variables and arrays
    integer,                       intent(out) :: levcfg, imcon, megatm
    real(dp),         allocatable, intent(out) :: xxx(:),yyy(:),zzz(:),cell(:)
    character(len=8), allocatable, intent(out) :: atmnam(:)

    !Local variables
    integer            :: ierr, dummy_index,ia
    real(dp)           :: vx,vy,vz,fx,fy,fz
    character(len=100) :: header 

    !-----------------
    !Main Routine 
    !----------------
    !See my notes on DL CONFIG file or DL manual 
    !See 'readTGeometryGen_help' in typegeometryhsd.F90

    !Open CONFIG
    open(unit=100,file='CONFIG',status='old',access='sequential',iostat=ierr)  
    if(ierr/=0)then
       write(stdOut,*) 'Can''t find DL POLY CONFIG file.'
       write(stdOut,*) 'read_DLPOLY_vars has stopped'
       stop
    endif

    !Line 1. Header info not used 
    read(100,*) header 

    !Line 2
    read(100,*) levcfg, imcon, megatm

    !imcon relates to the boundary conditions but for now, using
    !finite boundaries with DFTB, regardless of DL input 

    !Lines 3-6. Supercell vectors
    allocate(cell(9))
    read(100,*) cell(1:3)
    read(100,*) cell(4:6)
    read(100,*) cell(7:9)

    !Lines 7 -> 7+(levcfg*megatm) 
    !i.e. the rest of the file
    allocate(atmnam(megatm),xxx(megatm),yyy(megatm),zzz(megatm))

    !Coordinates only
    if(levcfg==0)then
       do ia=1,megatm
          read(100,*) atmnam(ia), dummy_index
          read(100,*) xxx(ia),yyy(ia),zzz(ia)
       enddo
    endif

    !Coordinates and velocities
    if(levcfg==1)then
       write(stdOut,*) 'CONFIG initial velocities not stored'
       do ia=1,megatm
          read(100,*) atmnam(ia), dummy_index
          read(100,*) xxx(ia),yyy(ia),zzz(ia)
          read(100,*) vx,vy,vz
       enddo
    endif

    !Coordinates, velocities and forces
    if(levcfg==2)then
       write(stdOut,*) 'CONFIG initial velocities & forces not stored'
       do ia=1,megatm
          read(100,*) atmnam(ia), dummy_index
          read(100,*) xxx(ia),yyy(ia),zzz(ia)
          read(100,*) vx,vy,vz
          read(100,*) fx,fy,fz
       enddo
    endif

  end subroutine read_DLPOLY_vars



  !*************************************
  !Output forces per time step
  !*************************************
  subroutine output_forces(itr, forces, units)
    use dftb_api, only: convert_unit
    implicit none

    !Declarations
    integer,               intent(in) :: itr
    real(dp), allocatable, intent(in) :: forces(:,:)
    character(len=*),      intent(in),   optional :: units
    integer           :: ia
    real(dp)          :: factor
    character(len=20) :: fname
    character(len=3)  :: itr_lab
    
    !Main Routine
    if(present(units))then
       if(trim(adjustl(units(1:2)))=='DL') factor=1._dp
       if(trim(adjustl(units(1:2)))=='DF') factor=convert_unit('DLPOLY','DFTB+','force') 
    elseif(.not. present(units))then
       factor=1._dp
    endif

    write(itr_lab,'(I3)') itr
    fname='forces_'//trim(adjustl(itr_lab))//'_'//trim(adjustl(units(1:2)))//'.dat'
    open(unit=100,file=trim(adjustl(fname)))
    write(100, "(A,I2,A)") 'Outputting total forces for time step, ',itr
    write(100,*) 'In units consistent with '//trim(adjustl(units))
   
    do ia=1,size(forces, dim=2)
       write(100, "(3F20.12)") forces(:, ia)*factor
    end do
    close(100)
    
  end subroutine output_forces

  
  !**********************************************************************************
  !Make slight, random perturbations to the atomic positions per time step ~ 3-5%
  !**********************************************************************************
  subroutine move_atomic_positions_rand(itr,xxx,yyy,zzz) 
    implicit none
    !--------------
    !Declarations
    !--------------
    !Modular
    integer,                       intent(in)    :: itr
    real(dp),         allocatable, intent(inout) :: xxx(:),yyy(:),zzz(:)
    !Local
    integer   :: ia
    real(dp)  :: dx,dy,dz,limit
    real(dp),    allocatable :: rn(:)

    !--------------
    !Main routine
    !--------------
    if(itr>1)then
       allocate(rn(3))
       limit=0.03_dp
       
       do ia=1,size(xxx)
          !Random displacements in range [-limit:limit]*atomic position
          call rand_number(rn)
          dx=(rn(1)-0.5_dp)*limit*xxx(ia)
          dy=(rn(2)-0.5_dp)*limit*yyy(ia)
          dz=(rn(3)-0.5_dp)*limit*zzz(ia)

          !Update atomic positions
          xxx(ia)=xxx(ia)+dx
          yyy(ia)=yyy(ia)+dy
          zzz(ia)=zzz(ia)+dz
       enddo
       
    endif
          
  end subroutine move_atomic_positions_rand

  !**********************************************************************************
  !Generate an array of random numbers, optionally in the range [min:max]
  !Note, if using this in a loop ~ 50 values will be the same due to the speed of iteration
  !w.r.t. change in clock
  !**********************************************************************************
  subroutine rand_number(array,min,max)
    implicit none
    integer :: i, n, clock,j
    integer, dimension(:), allocatable :: seed
    real(dp), allocatable :: array(:)
    real(dp), optional ::min,max 
    call random_seed(size = n)
    allocate(seed(n))
    call system_clock(count=clock)
    seed = clock + 37 * (/ (i - 1, i = 1, n) /)
    call random_seed(put = seed)
    call random_number(array)
    if(present(min) .and. present(max)) array = (array*(max-min))+min
    deallocate(seed)
  end subroutine rand_number

  
  
end program call_lib
