!**************************************************************************************************
! Test DFTB library API for DL POLY. V1. March 2018
! Alexander Buccheri. CCC UoB. ab17369@bristol.ac.uk 
!
! Read in DL CONFIG file, assign data to DFTB+ geometry object and pass to DFTB+ to do calculation
!
!**************************************************************************************************

program call_lib
  !Precision and types 
  !I disagree with definition of dp but using it to 
  !test against DFTB+ running as a program
  use accuracy,       only: dp,mc
  use io,             only: stdOut
  use typegeometry,   only: TGeometry

  !Library and API
  use dftb_libwrapper,only: dftbplus_app
  use dftb_api,       only: fill_DFTB_geometry,  print_geometry_data
  implicit none

  
  !-----------------
  !Declarations
  !-----------------
  !DL Poly CONFIG data
  integer                       :: levcfg, imcon, megatm
  real(dp),         allocatable :: xxx(:),yyy(:),zzz(:),cell(:)
  character(len=3), allocatable :: atmnam(:)

  !Modular variables/arrays/objects  
  integer               :: itr, NtimeSteps
  type(TGeometry)       :: geo
  real(dp), allocatable :: forces(:,:)
  
  !-----------------
  !Main Routine
  !-----------------
  !Assign DLPOLY variables: In test, read from config.
  !When used in DLPOLY, variables and arrays will be in scope
  call read_DLPOLY_vars(levcfg, imcon, megatm, cell, xxx,yyy,zzz, atmnam)
 
  !Put DL POLY data into DFTB+ objects
  call fill_DFTB_geometry(levcfg, imcon, megatm, cell, xxx,yyy,zzz, atmnam, geo)
  call print_geometry_data(geo)

  !Call dftb+ library 
  !Note, need a way to update atomic coordinates for time steps>1
  NtimeSteps=1  
  do itr=1,NtimeSteps
     call dftbplus_app(itr,geo, forces)
     !call output_forces(itr, forces)
  enddo


  
contains

  !**********************************************************************************
  ! When using the DFTB+ library in DLPOLY, there will be no need for this subroutine
  !**********************************************************************************
  subroutine read_DLPOLY_vars(levcfg, imcon, megatm, cell, xxx,yyy,zzz, atmnam)
    implicit none

    !-----------------
    !Declarations
    !----------------
    !Modular variables and arrays
    integer,                       intent(out) :: levcfg, imcon, megatm
    real(dp),         allocatable, intent(out) :: xxx(:),yyy(:),zzz(:),cell(:)
    character(len=3), allocatable, intent(out) :: atmnam(:)

    !Local variables
    integer            :: ierr, dummy_index,ia
    real(dp)           :: vx,vy,vz,fx,fy,fz
    character(len=100) :: header 

    !-----------------
    !Main Routine 
    !----------------
    !See my notes on DL CONFIG file or DL manual 
    !See 'readTGeometryGen_help' in typegeometryhsd.F90

    !Open CONFIG
    open(unit=100,file='CONFIG',status='old',access='sequential',iostat=ierr)  
    if(ierr/=0)then
       write(stdOut,*) 'Can''t find DL POLY CONFIG file.'
       write(stdOut,*) 'read_DLPOLY_vars has stopped'
       stop
    endif

    !Line 1. Header info not used 
    read(100,*) header 

    !Line 2
    read(100,*) levcfg, imcon, megatm

    !imcon relates to the boundary conditions but for now, using
    !finite boundaries with DFTB, regardless of DL input 

    !Lines 3-6. Supercell vectors
    allocate(cell(9))
    read(100,*) cell(1:3)
    read(100,*) cell(4:6)
    read(100,*) cell(7:9)

    !Lines 7 -> 7+(levcfg*megatm) 
    !i.e. the rest of the file
    allocate(atmnam(megatm),xxx(megatm),yyy(megatm),zzz(megatm))

    !Coordinates only
    if(levcfg==0)then
       do ia=1,megatm
          read(100,*) atmnam(ia), dummy_index
          read(100,*) xxx(ia),yyy(ia),zzz(ia)
       enddo
    endif

    !Coordinates and velocities
    if(levcfg==1)then
       write(stdOut,*) 'CONFIG initial velocities not stored'
       do ia=1,megatm
          read(100,*) atmnam(ia), dummy_index
          read(100,*) xxx(ia),yyy(ia),zzz(ia)
          read(100,*) vx,vy,vz
       enddo
    endif

    !Coordinates, velocities and forces
    if(levcfg==2)then
       write(stdOut,*) 'CONFIG initial velocities & forces not stored'
       do ia=1,megatm
          read(100,*) atmnam(ia), dummy_index
          read(100,*) xxx(ia),yyy(ia),zzz(ia)
          read(100,*) vx,vy,vz
          read(100,*) fx,fy,fz
       enddo
    endif

  end subroutine read_DLPOLY_vars



  !*************************************
  !Output forces per time step
  !*************************************
  subroutine output_forces(itr, forces)
    implicit none

    !Declarations
    integer, intent(in) :: itr
    real(dp), allocatable, intent(in) :: forces(:,:)
    integer :: ia
    
    !Main Routine 
    write(*, "(A,I2,A)") 'Outputting total forces for time step, ',itr,':'
    do ia=1,size(forces, dim=2)
       write(*, "(3F20.12)") forces(:, ia)
    end do
    write(*,*)
  
  end subroutine output_forces

  
end program call_lib
