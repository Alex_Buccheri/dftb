#!/usr/bin/env python3

#--------------------------------------------------------------------------------
#   Interface test, during which atomic coordinates are converted from Ang to Bohr
#
#   Description:
#
#	Use the code to generate an output for the geometry 
#	Copy the dftb_in.hsd and add the geometry to it via referencing external file: output.gen

#	Run call.exe and output forces (from DFTB+ rather than from my interface)
#	Run DFTB+ using the generated dftb_in.hsd
#	Compare the outputs
#--------------------------------------------------------------------------------

#----------------------------------
#Libraries and modules
#----------------------------------
#Libraries 
import sys
import numpy as np
import os
import shutil
import subprocess


#Functions 
def line_prepender(filename, line):
    with open(filename, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + content)


#-----------------------------------
#Directory paths for user to set:
#-----------------------------------
DFTB_ROOT="/Users/alexanderbuccheri/Codes/development/dftb_17.1"
INPUT_FILE_ROOT='2.calltest'
EXE=DFTB_ROOT+'/_build/prog/dftb+/dftb+'


#-----------------------------------
# Main Routine
#-----------------------------------
TEST_ROOT=os.popen('pwd').read()

#Calculation with library interface
print('Copying input files from '+DFTB_ROOT+'/'+INPUT_FILE_ROOT+' to lib')
os.mkdir('lib')
shutil.copy2(DFTB_ROOT+'/'+INPUT_FILE_ROOT+'/CONFIG',      'lib/')
shutil.copy2(DFTB_ROOT+'/'+INPUT_FILE_ROOT+'/CONTROL',     'lib/')
shutil.copy2(DFTB_ROOT+'/'+INPUT_FILE_ROOT+'/FIELD',       'lib/')
shutil.copy2(DFTB_ROOT+'/'+INPUT_FILE_ROOT+'/dftb_in.hsd', 'lib/')
os.chdir('lib')
print('Running local build of call.exe') 
os.system('../build/call.exe > terminal.out')
os.chdir('../')

#Run DFTB+ program with dftb_in.hsd generated from library calculation
print('Copying dftb_in.hsd used with interface calculation and .gen positions returned \
from interface calculation to prog')
os.mkdir('prog')
shutil.copy2('lib/dftb_in.hsd', 'prog/')
shutil.copy2('lib/output.gen', 'prog/')  #Assume DFTB+ outputs in Ang as it expects it in in Ang. 
os.chdir('prog')

print('Modifying dftb_in.hsd to use output.gen for atomic positions')
geo_string = \
'Geometry = GenFormat {    \n'  +\
'    <<< "output.gen"      \n'  +\
'} '
line_prepender('dftb_in.hsd', geo_string)

#Run dftb+ executable (not library) using input script and coordinates from library test
print('Running release version of dftb+ (different executable) with positions returned \
from the interface')
os.system(EXE+' > terminal.out')

#Compare outputs with kdiff myself
os.chdir('../')
print(' Use: kdiff3 lib/terminal.out prog/terminal.out   in the terminal to compare outputs')
