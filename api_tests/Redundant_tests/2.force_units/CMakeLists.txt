#------------------------------------------------
#CMake for DFTB+
#------------------------------------------------

cmake_minimum_required(VERSION 3.02)
if(COMMAND cmake_policy)
  cmake_policy(SET CMP0048 NEW)
endif(COMMAND cmake_policy)

set(DFTB_ROOT ../..)


project(dftbplus 
  VERSION 17.1
  LANGUAGES Fortran C)
set(CDEF "-DDEBUG=0 -DWITH_SOCKETS ")
set(FPP ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/external/fypp/bin/fypp)
set(FPP_FLAGS "-DDEBUG=0 -DWITH_SOCKETS ")


include_directories(${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include)

include(GNUInstallDirs)
set(LIBRARY_OUTPUT_PATH ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})


#------------------------------------------------
#Compile external XML and fsockets libraries 
#------------------------------------------------
set(XMLSRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/external/xmlf90)
add_library(libxmlf90 SHARED
${XMLSRC}/flib_dom.f90
${XMLSRC}/m_dom_nodelist.f90
${XMLSRC}/m_dom_types.f90
${XMLSRC}/m_strings.f90
${XMLSRC}/m_dom_utils.f90
${XMLSRC}/m_dom_document.f90
${XMLSRC}/m_dom_namednodemap.f90
${XMLSRC}/m_dom_node.f90
${XMLSRC}/m_dom_error.f90
${XMLSRC}/m_dom_debug.f90
${XMLSRC}/flib_wxml.f90
${XMLSRC}/m_wxml_core.f90
${XMLSRC}/m_wxml_elstack.f90
${XMLSRC}/m_wxml_buffer.f90
${XMLSRC}/m_wxml_dictionary.f90
${XMLSRC}/m_wxml_text.f90
${XMLSRC}/m_dom_element.f90
${XMLSRC}/m_dom_attribute.f90
${XMLSRC}/m_dom_parse.f90
${XMLSRC}/flib_sax.f90
${XMLSRC}/m_xml_parser.f90
${XMLSRC}/m_elstack.f90
${XMLSRC}/m_buffer.f90
${XMLSRC}/m_fsm.f90
${XMLSRC}/m_entities.f90
${XMLSRC}/m_dictionary.f90
${XMLSRC}/m_charset.f90
${XMLSRC}/m_debug.f90
${XMLSRC}/m_reader.f90
${XMLSRC}/m_io.f90
${XMLSRC}/m_xml_error.f90
${XMLSRC}/m_converters.f90
 )

set(FSOCKETSSRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/external/fsockets)
add_library(extfsockets SHARED ${FSOCKETSSRC}/sockets.c ${FSOCKETSSRC}/fsockets.f90)


#------------------------------------------------
# Alex's API modules. Doesn't require pre-processing 
#------------------------------------------------
set(APISRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/api)
set(APISRC_FILES dftb_api)
file(COPY ${APISRC}/${APISRC_FILES}.f90
     DESTINATION ${PROJECT_BINARY_DIR})



#------------------------------------------------
#List all source codes in each lib directory
#------------------------------------------------
set(CSRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_common)



set(CSRC_FILES
  accuracy
  assert
  constants
  memman
  optarg
  unitconversion)

set(DESRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_derivs)
set(DESRC_FILES numderivs2)

 
# dispdftd3

set(DFSRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_dftb)
set(DFSRC_FILES
  chargeconstr
  charges
  coulomb
  densitymatrix
  dftbplusu
  dispcommon
  dispersions
  dispiface
  dispslaterkirkw
  dispuffdata
  dispuff
  elecconstraints
  emfields
  energies
  etemp
  externalcharges
  forces
  nonscc
  orbitalequiv
  periodic
  pmlocalisation
  populations
  potentials
  repcont
  reppoly
  repspline
  repulsive
  scc
  sccinit
  shift
  shortgamma
  sk
  slakocont
  slakoeqgrid
  sparse2dense
  spin
  spinorbit
  stress
  thirdorder)


#dftd3
set(EXSRC  ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_extlibs)
set(EXSRC_FILES 
  arpack
  blas
  fsockets
  lapack
  xmlf90)

set(GEOSRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_geoopt)
set(GEOSRC_FILES 
  conjgrad
  gdiis
  geoopt
  linemin
  steepdesc)

set(ISRC ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_io)
set(ISRC_FILES
  charmanip
  fileid
  formatout
  hsdparser
  hsdutils
  hsdutils2
  intrinsicpr
  io
  ipisocket
  logger
  message
  taggedoutput
  tokenreader
  xmlutils)

set(MASRC  ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_math)
set(MASRC_FILES   
  angmomentum
  bisect
  blasroutines
  eigensolver
  erfcalc
  errorfunction
  factorial
  hermite
  interpolation
  lapackroutines
  qm
  randomgenpool
  ranlux
  simplealgebra
  sorting)

set(MDSRC  ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_md)
set(MDSRC_FILES
  andersentherm
  berendsentherm
  dummytherm
  extlagrangian
  mdcommon
  mdintegrator
  nhctherm
  tempprofile
  thermostat
  velocityverlet
  xlbomd)

set(MISRC  ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_mixer)
set(MISRC_FILES
  andersonmixer
  broydenmixer
  diismixer
  mixer
  simplemixer)

set(TISRC  ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_timedep)
set(TISRC_FILES
  linrespcommon
  linresp
  linrespgrad)

set(TYSRC  ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/lib_type)
set(TYSRC_FILES
  commontypes
  fifo
  linkedlist
  linkedlisti0
  linkedlisti1
  linkedlistlc0
  linkedlistmc0
  linkedlistr0
  linkedlistr1
  linkedlistr2
  linkedlists0
  oldskdata
  typegeometry
  typegeometryhsd)

set(PRSRC  ${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/prg_dftb)
set(PRSRC_FILES
  dftb_libwrapper
  eigenvects
  initprogram
  inputdata
  main
  mainio
  oldcompat
  parser)



#----------------------------------------
#Preprocess ALL source in each directory, 
#regardless of whether it's required
#----------------------------------------

foreach(pcr ${CSRC_FILES})
  add_custom_target(${pcr} COMMAND ${FPP} -DDEBUG=0 -DWITH_SOCKETS   -I${CSRC} -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${CSRC}/${pcr}.F90 > ${pcr}.f90 
  WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  SET_SOURCE_FILES_PROPERTIES(${pcr}.f90 PROPERTIES GENERATED TRUE)
  message("${FPP}  -DDEBUG=0 -DWITH_SOCKETS   -I${CSRC} -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${CSRC}/${pcr}.F90 > ${pcr}.f90")
endforeach(pcr)


message(${PROJECT_BINARY_DIR})

#[[
FOREACH(pcr ${CSRC_FILES})
   set(infilename ${CSRC}/${pcr}.F90)
   string(REGEX REPLACE ".F90\$" ".f90" outfilename "${infilename}")

   set(outfile "${CMAKE_CURRENT_BINARY_DIR}/${pcr}.f90")
   message(${outfile})

   set(infile "${CMAKE_CURRENT_SOURCE_DIR}/${infilename}")
   message(${infile})

   add_custom_command(
       OUTPUT "${outfile}" 
      COMMAND ${FPP} "-DDEBUG=0 -DWITH_SOCKETS " -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/${CSRC} -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${infile} > ${outfile} 
       MAIN_DEPENDENCY "${infile}"
       VERBATIM	       
       )

    message("${FPP} -DDEBUG=0 -DWITH_SOCKETS   -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/${CSRC} -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${infile} > ${outfile}")
   set(outFiles ${outFiles} " ${outfile}")

endforeach(pcr)

message( ${outFiles} )
]]





foreach(pcr ${ISRC_FILES})
  add_custom_target(${pcr} COMMAND ${FPP} -DDEBUG=0 -DWITH_SOCKETS   -I${ISRC}
    -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${ISRC}/${pcr}.F90 >
  ${pcr}.f90 WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  SET_SOURCE_FILES_PROPERTIES(${pcr}.f90 PROPERTIES GENERATED TRUE)
endforeach(pcr)

foreach(pcr ${DESRC_FILES})
  add_custom_target(${pcr} COMMAND ${FPP} -DDEBUG=0 -DWITH_SOCKETS   -I${DESRC}
    -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${DESRC}/${pcr}.F90 >
  ${pcr}.f90 WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  SET_SOURCE_FILES_PROPERTIES(${pcr}.f90 PROPERTIES GENERATED TRUE)
endforeach(pcr)

foreach(pcr ${DFSRC_FILES})
  add_custom_target(${pcr} COMMAND ${FPP} -DDEBUG=0 -DWITH_SOCKETS   -I${DFSRC}
    -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${DFSRC}/${pcr}.F90 >
  ${pcr}.f90 WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  SET_SOURCE_FILES_PROPERTIES(${pcr}.f90 PROPERTIES GENERATED TRUE)
endforeach(pcr)

foreach(pcr ${EXSRC_FILES})
  add_custom_target(${pcr} COMMAND ${FPP} -DDEBUG=0 -DWITH_SOCKETS   -I${EXSRC}
    -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${EXSRC}/${pcr}.F90 >
  ${pcr}.f90 WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  SET_SOURCE_FILES_PROPERTIES(${pcr}.f90 PROPERTIES GENERATED TRUE)
endforeach(pcr)

foreach(pcr ${GEOSRC_FILES})
  add_custom_target(${pcr} COMMAND ${FPP} -DDEBUG=0 -DWITH_SOCKETS   -I${GEOSRC}
    -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${GEOSRC}/${pcr}.F90 >
  ${pcr}.f90 WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  SET_SOURCE_FILES_PROPERTIES(${pcr}.f90 PROPERTIES GENERATED TRUE)
endforeach(pcr)

foreach(pcr ${MASRC_FILES})
  add_custom_target(${pcr} COMMAND ${FPP} -DDEBUG=0 -DWITH_SOCKETS   -I${MASRC}
    -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${MASRC}/${pcr}.F90 >
  ${pcr}.f90 WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  SET_SOURCE_FILES_PROPERTIES(${pcr}.f90 PROPERTIES GENERATED TRUE)
endforeach(pcr)

foreach(pcr ${MDSRC_FILES})
  add_custom_target(${pcr} COMMAND ${FPP} -DDEBUG=0 -DWITH_SOCKETS   -I${MDSRC}
    -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${MDSRC}/${pcr}.F90 >
  ${pcr}.f90 WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  SET_SOURCE_FILES_PROPERTIES(${pcr}.f90 PROPERTIES GENERATED TRUE)
endforeach(pcr)

foreach(pcr ${MISRC_FILES})
  add_custom_target(${pcr} COMMAND ${FPP} -DDEBUG=0 -DWITH_SOCKETS   -I${MISRC}
    -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${MISRC}/${pcr}.F90 >
  ${pcr}.f90 WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  SET_SOURCE_FILES_PROPERTIES(${pcr}.f90 PROPERTIES GENERATED TRUE)
endforeach(pcr)

foreach(pcr ${TISRC_FILES})
  add_custom_target(${pcr} COMMAND ${FPP} -DDEBUG=0 -DWITH_SOCKETS   -I${TISRC}
    -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${TISRC}/${pcr}.F90 >
  ${pcr}.f90 WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  SET_SOURCE_FILES_PROPERTIES(${pcr}.f90 PROPERTIES GENERATED TRUE)
endforeach(pcr)

foreach(pcr ${TYSRC_FILES})
  add_custom_target(${pcr} COMMAND ${FPP} -DDEBUG=0 -DWITH_SOCKETS    -I${TYSRC}
    -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${TYSRC}/${pcr}.F90 >
  ${pcr}.f90 WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  SET_SOURCE_FILES_PROPERTIES(${pcr}.f90 PROPERTIES GENERATED TRUE)
endforeach(pcr)

foreach(pcr ${PRSRC_FILES})
  add_custom_target(${pcr} COMMAND ${FPP} -DDEBUG=0 -DWITH_SOCKETS    -I${PRSRC}
    -I${PROJECT_SOURCE_DIR}/${DFTB_ROOT}/prog/dftb+/include ${PRSRC}/${pcr}.F90 >
  ${pcr}.f90 WORKING_DIRECTORY ${PROJECT_BINARY_DIR})
  SET_SOURCE_FILES_PROPERTIES(${pcr}.f90 PROPERTIES GENERATED TRUE)
endforeach(pcr)





#------------------------
#Add all .f90 to library
#------------------------



set(SOURCE_LIST  ${CSRC_FILES} ${DESRC_FILES} ${DFSRC_FILES} ${EXSRC_FILES} 
 ${GEOSRC_FILES} ${ISRC_FILES} ${MASRC_FILES} ${MDSRC_FILES} ${MISRC_FILES}
 ${TISRC_FILES} ${TYSRC_FILES} ${PRSRC_FILES} ${APISRC_FILES})

#MESSAGE("    " )
#foreach(v ${SOURCE_LIST})
#  MESSAGE( ${v})
#endforeach()

#Note, 'var arg' is the suffix and 'suffix arg' is the list
FUNCTION(EXTEND var suffix)
   SET(listVar "")
   FOREACH(f ${ARGN})
      LIST(APPEND listVar "${f}${suffix}")
   ENDFOREACH(f)
   SET(${var} "${listVar}" PARENT_SCOPE)
ENDFUNCTION(EXTEND)

EXTEND(SOURCE_CODE ".f90" ${SOURCE_LIST})

#set(SOURCE_CODE ${SOURCE_CODE} "${outFiles}")


#MESSAGE("    " )
#foreach(v ${SOURCE_CODE})
#  MESSAGE( ${v})
#endforeach()
find_package(LAPACK REQUIRED)

add_library(dftb SHARED ${SOURCE_CODE} )
add_dependencies(dftb ${SOURCE_LIST} libxmlf90 extfsockets)
target_link_libraries(dftb libxmlf90 extfsockets ${LAPACK_LIBRARIES})
target_compile_definitions(dftb PRIVATE ${CDEF})
set_target_properties(dftb PROPERTIES VERSION ${PROJECT_VERSION} SOVERSION 1)



#FIND_LIBRARY(ARPACK_LIBRARY libarpack_GCC-6.1.a)
#INCLUDE(FindPackageHandleStandardArgs)
#FIND_PACKAGE_HANDLE_STANDARD_ARGS(ARPACK DEFAULT_MSG ARPACK_LIBRARY)

add_executable(call.exe call_lib.f90 ${APISRC_FILES}.f90)

target_link_libraries(call.exe dftb libxmlf90 extfsockets ${LAPACK_LIBRARIES})

#target_link_libraries(call.exe dftb libxmlf90 extfsockets ${LAPACK_LIBRARIES} ${ARPACK_LIBRARY})
