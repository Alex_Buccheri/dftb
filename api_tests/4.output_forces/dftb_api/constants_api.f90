module constants_api
  use kinds_f90, only: wp
  implicit none

  !----------------------
  !Unit conversions
  !----------------------
  !> E0 -> J (== 10 J/mol) ref pg 7 DL POLY Manual 
  real(wp), parameter :: EMolecular__J = 1.6605402e-23_wp

  !> Bohr->Angstrom
  real(wp), parameter :: Bohr__AA = 0.529177249_wp

  !> Angstrom->Bohr
  real(wp), parameter :: AA__Bohr = 1.0_wp / Bohr__AA

   !> Hartree -> Joule
  real(wp), parameter :: Hartree__J = 4.3597441775e-18_wp
  
  !----------------------
  !Constants
  !----------------------
  !DLPOLY time step starts on 0 
  integer, parameter  :: initial_MD_step = 0
  
end module constants_api
