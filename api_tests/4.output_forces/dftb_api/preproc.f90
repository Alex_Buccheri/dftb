module preproc

contains

  !Use preprocessing variable to set dftb usage flag
  !Note, doesn't need to be subroutine, could just be
  !in constants_api.F90
  subroutine set_dftb_flag(use_dftb)
    implicit none
    logical, intent(out) :: use_dftb
    use_dftb = .true.
  end subroutine set_dftb_flag
  

end module preproc
