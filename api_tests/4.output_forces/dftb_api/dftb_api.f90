!*******************************************************************************************************
! DL POLY 4.09 interface for force calculation with DFTB+ V17.1     
! Allows DL POLY to use DFTB+ as a method of force calculation in MD Simulations
!
! Subroutines in this module should only use the DFTB+ routines:  
!  dftb_libwrapper       Allows DFTB+ v17 to be called as a library
!  typegeometry          Contains the DFTB+ geometry type (consider copying to DLPOLY)
!
! Alexander Buccheri, University of Bristol 2018
! ab17369@bristol.ac.uk 
!*******************************************************************************************************
module dftb_api
  use kinds_f90, only: wp
  
  !DFTB+ library modules
  use typegeometry,    only: TGeometry

  private 
  public  ::  w_calculate_dftb_forces
  
contains
  
  !***************************************************************************************************
  ! Description:
  !   Puts DL POLY data into types used by DFTB+, calls DFTB+ for a force calculation and converts
  !   the forces returned into internal units used by DL POLY
  !---------------------------------------------------------------------------------------------------
  ! Inputs:
  !   nstep                                    MD Time step index (starting at 0) 
  !   nrite                                    ? Not required 
  !   levcfg                                   CONFIG File Key 
  !   imcon                                    Boundary Key
  !   megatm                                   Total number of atoms in system
  !                                            (should I be using 'mxatms' instead?)
  !   cell(9)                                  cell(1:3) = (x,y,z) component of 'a' cell vector (unit?)
  !                                            cell(4:6) = 'b' cell vector, cell(7:9) 'c' cell vector 
  !   xxx(megatm),yyy(megatm),zzz(megatm)      x,y and z position vectors (Ang)
  !   atmnam(megatm)                           Name of each atom in system  
  !   unqatm(ntpatm?)                          List of unique atoms in the system       
  !                                            Currently unused by API
  ! Outputs:
  !   fxx(megatm),fyy(megatm),fzz(megatm)      Force acting on (x,y,z) component of atoms 1:megam
  !                                            (Units of E0/Ang)
  !***************************************************************************************************
  subroutine w_calculate_dftb_forces(nstep,nrite, levcfg,imcon,megatm,cell,xxx,yyy,zzz,atmnam,unqatm,&
                                     fxx,fyy,fzz)
    !DFTB+ Library 
    use dftb_libwrapper, only: dftbplus_app
    !DLPOLY_DFTB+ 
    use utilities,       only: convert_unit,output_forces
    use constants_api,   only: initial_MD_step
    implicit none 

    !-----------------
    !Declarations
    !-----------------
    !DL POLY 
    Integer,                        intent(in) :: nstep,nrite,levcfg,imcon,megatm
    Real(wp),                       intent(in) :: cell(9)
    Real(wp),          allocatable, intent(in) :: xxx(:), yyy(:),zzz(:)
    Character(Len=8),  allocatable, intent(in) :: atmnam(:)
    Character(Len=*),  allocatable, intent(in) :: unqatm(:)
    Real(wp),          allocatable, intent(inout) :: fxx(:),fyy(:),fzz(:)
    !DFTB+
    type(TGeometry)       :: geo

    !Local
    real(wp), allocatable :: forces(:,:)
    
    !------------------
    !Main Routine
    !------------------
    !Initialise force arrays 
    fxx = 0.0_wp ; fyy = 0.0_wp ; fzz = 0.0_wp
    
    !Fill DFTB+ object with DLPOLY data 
    if(nstep==initial_MD_step)then
       call check_dftb_input_file()
       write(*,*)'Initialising DFTB+ geometry from CONFIG data'
       call fill_DFTB_geometry(levcfg, imcon, megatm, cell, xxx,yyy,zzz, atmnam, geo)
    elseif(nstep>initial_MD_step)then
       call update_DFTB_geometry(megatm,xxx,yyy,zzz, geo)
    endif

    !-----------------------------------------
    !For testing
    !-----------------------------------------
    !Output geometry in .gen format
    !DLPOLY data in Ang. .gen data requires Ang, hence no conversion required
    call output_DLPOLY_geometry_data('gen', levcfg, imcon, megatm, cell, xxx,yyy,zzz, &
                                     atmnam, nstep)
    
    !Calculate forces 
    if(.not. allocated(forces)) allocate(forces(3,megatm))
    write(*,*) 'MD step: ',nstep
    call dftbplus_app(initial_MD_step,nstep,geo, forces)

    
    !Output forces per MD step for interface testing
    call output_forces(nstep,forces,'DFTB')

    fxx=forces(1,:)*convert_unit('DFTB','DLPOLY','force') 
    fyy=forces(2,:)*convert_unit('DFTB','DLPOLY','force') 
    fzz=forces(3,:)*convert_unit('DFTB','DLPOLY','force') 
    
  end subroutine w_calculate_dftb_forces

  

  
  !************************************************************************************************************
  ! Put DL POLY data (4.09) into DFTB+ (17.1) geometry object  
  !************************************************************************************************************
  ! DFTB+ Internal Units
  !   Length    Bohr 
  !
  ! DL Internal Units
  !   Length   Angstrom
  !
  !------------------------------------------------------------------------------------------------------------
  ! Type defined in DFTB+: typegeometry.F90 
  ! Geometry type contains:
  !
  !     integer  :: nAtom             Number of atoms in system
  !     real(wp) :: coords(3,NAtom)   Atomic coordinates (in Bohr)
  !     integer  :: species(nAtom)    Species index: Each integer corresponds to an element in speciesNames
  !     integer  :: nSpecies          Number of atomic species in the system
  !     character:: speciesNames(nSpecies) Array of species strings, for each element in the system
  !
  !     logical  :: tPeriodic         Is system periodic
  !                                   If yes, lattice vectors must be set
  !     real(wp) :: latVecs(3,3)      Lattice vectors, stored row-wise 
  !                                   Currently set to DL POLY's supercell vectors (DL units)
  !     real(wp) :: recVecs2p(3,3)    Reciprocal lattice vectors 
  !                                   Not set by this routine due to choice of boundary conditions
  !     logical  :: tFracCoord        If system is periodic, one can specify basis atomic positions
  !                                   in fractional coordinates (unit/fractions of the lattice vectors)
  !                                   Should always be false to be consistent with DL internal units
  !     real(wp) :: origin(3)         Only required for periodic boundaries, hence not defined 
  !
  !-----------------------------------------------------------------------------------------------------------
  ! To Does: Generalise to periodic boundary conditions 
  ! Boundary conditions:   Hard-coded to finite
  ! latVecs:               CHECK if row or column-wise (can print from readTGeometryHSD) then convert to Bohr
  ! recVecs2p:             Compute from latVecs and store in units of 2 pi
  ! origin:                Add and ensure converted to Bohr 
  !***********************************************************************************************************
  subroutine fill_DFTB_geometry(levcfg, imcon, megatm, cell, xxx,yyy,zzz, atmnam, geo)
    !DFTB+ modules 
    use constants_api,    only: AA__Bohr
    implicit none

    !-----------------
    !Declarations
    !----------------
    !Modular variables and arrays
    integer,                       intent(in) :: levcfg, imcon, megatm
    real(wp),                      intent(in) :: cell(9)
    real(wp),         allocatable, intent(in) :: xxx(:),yyy(:),zzz(:) 
    character(len=8), allocatable, intent(in) :: atmnam(:)

    !Modular object 
    type(TGeometry), intent(out) :: geo

    !Local variables and arrays
    integer           :: ia,ja,cnt
    logical           :: already_found
    character(len=1)  :: boundary
    character(len=3), allocatable :: species_names(:),unique_species_names(:)


    !-----------------------
    !Main Routine
    !-----------------------
    !Assume always want finite boundary conditions with DFTB+ calculation 
    boundary='c'

    !Boundary conditions and units 
    select case (boundary)
    case("S","s")
       geo%tPeriodic = .true.
       geo%tFracCoord = .false.
    case("F","f")
       geo%tPeriodic = .true.
       geo%tFracCoord = .true.
    case("C", "c")
       geo%tPeriodic = .false.
       geo%tFracCoord = .false.
    case default
       write(*,*) 'Boundary condition not recognised.'
       write(*,*) 'Code has stopped'
       stop
    end select

    !Number of atoms 
    geo%nAtom=megatm

    !imcon relates to the boundary conditions but for now, using
    !finite boundaries with DFTB, regardless of DL input 

    write(*,*) 'Assigned DL supercell vectors to DFTB+ lattice vectors'
    if(.not. allocated(geo%latVecs)) allocate(geo%latVecs(3, 3))
    geo%latVecs(:, 1) = cell(1:3)
    geo%latVecs(:, 2) = cell(4:6)
    geo%latVecs(:, 3) = cell(7:9)

    if(.not. allocated(geo%species))   allocate(geo%species(geo%nAtom))
    if(.not. allocated(geo%coords))    allocate(geo%coords(3, geo%nAtom))
    allocate(species_names(geo%nAtom))
    geo%species=0
    geo%coords=0._wp
    species_names='   '

    !Coordinates and species of atom at each coordinate
    do ia=1,geo%nAtom
       species_names(ia) = atmnam(ia)
       geo%coords(1:3,ia)=(/xxx(ia),yyy(ia),zzz(ia)/) * AA__Bohr
    enddo

    !Identify the number of unique atom species in the system 
    !Allocated with upper bound 
    allocate(unique_species_names(118))   
    unique_species_names(1)=species_names(1)
    cnt=1
    already_found=.false.

    do ja=1,geo%nAtom
       do ia=1,cnt
          if( trim(adjustl( unique_species_names(ia) )) == &     
               trim(adjustl( species_names(ja) )) )then
             already_found=.true.
          endif
       enddo
       if(already_found.eqv..false.)then
          cnt=cnt+1
          unique_species_names(cnt)=species_names(ja)
       endif
       already_found=.false.
    enddo


    !Number of different atomic species 
    geo%nSpecies=cnt
    if(.not. allocated(geo%speciesNames)) allocate(geo%speciesNames(geo%nSpecies))
    geo%speciesNames=unique_species_names(1:geo%nSpecies)

    !Assign unique integer to each species
    do ia=1,geo%nAtom
       do ja=1,geo%nSpecies
          if( trim(adjustl(species_names(ia))) ==  &
               trim(adjustl(geo%speciesNames(ja))) )then
             geo%species(ia)=ja
             exit
          endif

       enddo
    enddo

    !Check species indices are consistent with species characters
    !do ia=1,megatm
    !   if( trim(adjustl(species_names(ia))) /=  &
    !       trim(adjustl(unique_species_names(geo%species(ia)))) )then
    !      write(*,*) 'Error found'
    !   endif
    !enddo

  end subroutine fill_DFTB_geometry


  !-----------------------------------------------------------------------------------------
  !Update atomic coordinates per MD step 
  !Expect the number of atoms to be conserved in this implememtation 
  !-----------------------------------------------------------------------------------------
  subroutine update_DFTB_geometry(megatm,xxx,yyy,zzz, geo)
    !DFTB+ modules 
    use constants_api,    only: AA__Bohr
    implicit none
    
    !-----------------
    !Declarations
    !------------------
    !Modular 
    integer,                       intent(in) :: megatm
    real(wp),         allocatable, intent(in) :: xxx(:),yyy(:),zzz(:) 
    type(TGeometry),               intent(out):: geo

    !Local
    integer :: ia
    
    !------------------
    !Main Routine 
    !------------------
    geo%nAtom=megatm
    
    if(.not. allocated(geo%coords)) allocate(geo%coords(3, geo%nAtom))
    geo%coords=0._wp
    
    do ia=1,geo%nAtom
       geo%coords(1:3,ia)=(/xxx(ia),yyy(ia),zzz(ia)/) * AA__Bohr
    enddo

    
  end subroutine update_DFTB_geometry
  

  !-------------------------------------------------------------
  !Output DLPOLY geometry data in .gen format
  !Primarily intended for testing purposes
  !-------------------------------------------------------------
  subroutine output_DLPOLY_geometry_data(form, levcfg, imcon, megatm, cell, xxx,yyy,zzz, &
                                         atmnam, nstep)
    use utilities, only: lowercase
    implicit none
    
    !-----------------
    !Declarations
    !-----------------
    !Modular variables and arrays
    integer,                       intent(in) :: levcfg, imcon, megatm, nstep
    real(wp),                      intent(in) :: cell(9)
    real(wp),         allocatable, intent(in) :: xxx(:),yyy(:),zzz(:) 
    character(len=8), allocatable, intent(in) :: atmnam(:)
    character(len=*),              intent(in) :: form
    
    !Local variables and arrays
    integer           :: ia,ja,cnt,nSpecies
    logical           :: already_found
    character(len=1)  :: boundary
    character(len=3)  :: sp_name
    character(len=5)  :: nstep_lab
    integer,             allocatable :: species_index(:)
    character(len=3),    allocatable :: species_names(:),unique_species_names(:)
    
    !-----------------
    !Main Routine 
    !-----------------
    !DFTB+ .gen format
    if(trim(adjustl(lowercase(form)))=='gen')then

       !---------------------------------------------------------------
       !Need to build some arrays now (DLPOLY probably already has them)
       !---------------------------------------------------------------
       !Species of atom at each coordinate
       allocate(species_names(megatm))
       species_names='  '
       do ia=1,megatm
          species_names(ia) = atmnam(ia)
       enddo
       
       !Identify the number of unique atom species in the system 
       !Allocated with upper bound 
       allocate(unique_species_names(118))   
       unique_species_names(1)=species_names(1)
       cnt=1
       already_found=.false.
       
       do ja=1,megatm
          do ia=1,cnt
             if( trim(adjustl( unique_species_names(ia) )) == &     
                  trim(adjustl( species_names(ja) )) )then
                already_found=.true.
             endif
          enddo
          if(already_found.eqv..false.)then
             cnt=cnt+1
             unique_species_names(cnt)=species_names(ja)
          endif
          already_found=.false.
       enddo
       nSpecies=cnt

       !Assign unique integer to each species
       allocate(species_index(megatm))
       species_index=0
       do ia=1,megatm
          sp_name=species_names(ia)
          do ja=1,nSpecies
             if( trim(adjustl(sp_name)) ==  &
                  trim(adjustl(unique_species_names(ja))) )then
                species_index(ia)=ja
                exit
             endif 
          enddo
       enddo
       

       !---------------------------------
       !Write to file
       !---------------------------------
       write(nstep_lab,'(I5)') nstep
       open(unit=100,file='structure_'//trim(adjustl(nstep_lab))//'.gen')

       !No need for unit conversion as DLPOLY internal is Ang, consistent with .gen requirement
       
       !Assume always want finite boundary conditions with DFTB+ calculation 
       boundary='c'
       
       !Lines 1 and 2
       write(100,'(A)')  '# Header line Atomic positions written out in Ang (required for gen format).'
       write(100,'(X,I3,X,A)') megatm, boundary

       !Line 3
       do ia=1,nSpecies
          write(100,'(A,X)',advance="NO") trim(adjustl(unique_species_names(ia)))
       enddo
       write(100,*)
       
       !Rest of file
       do ia=1,megatm
          write(100,'(I5,X,I3,X,3(f10.6,X))') ia,species_index(ia), xxx(ia),yyy(ia),zzz(ia)
       enddo
       
       close(100)

    !DLPOLY config format
    elseif(trim(adjustl(lowercase(form)))=='config')then
       write(*,*) 'Outputting in config format has not been written'
       write(*,*) 'Code stops'
       stop
   
    else
       write(*,*) 'Output format for DLPOLY geometry data is not recognised: ',trim(adjustl(form))
       write(*,*) 'Code stops'
       stop
    endif
    
  end subroutine output_DLPOLY_geometry_data


  !--------------------------------------------------------------------------
  !Checks to ensure that geometry details are not present in dftb_in.hsd, as
  !it will cause the code to crash (DLPOLY provides geometry)
  !--------------------------------------------------------------------------
  subroutine check_dftb_input_file()
    use utilities, only: lowercase,number_of_lines
    implicit none

    !Declarations
    character(len=11) :: fname
    character(len=60) :: line
    character(len=8)  :: substring
    integer           :: N,i,noccurrences,ios
    logical           :: exist

    !Main Routine
    write(*,*) 'Checking dftb_in.hsd'
    fname='dftb_in.hsd'
    substring='geometry'
    noccurrences=0

    inquire(file=trim(adjustl(fname)), exist=exist)

    if(exist)then
       N=number_of_lines(fname)
       open(unit=100, file=trim(adjustl(fname)), form='formatted', status='old', iostat=ios)
       do i=1,N
          read(100, '(A)') line
          noccurrences = noccurrences + index(lowercase(line), substring)
       enddo
       close(100)
       
    elseif(.not. exist)then
       write(*,*) 'Error openning: '//trim(adjustl(fname))
       write(*,*) 'DLPOLY has stopped'
       stop
    endif
    
    if(noccurrences>0)then
       write(*,*) 'Geometry should not be present in dftb_in.hsd'
       write(*,*) 'This information is obtained from the CONFIG file'
       write(*,*) 'DL POLY has stopped'
       stop
    endif
  
  end subroutine check_dftb_input_file


  subroutine preprocessing_warning()
     implicit none
     write(*,*) 'Warning, if USE_DFTB=OFF, w_calculate_dftb_forces in dftb_api.F90 should not get called'
     write(*,*) 'Code has stopped'
     stop
   end subroutine preprocessing_warning
  

end module dftb_api
