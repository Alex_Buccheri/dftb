module utilities
  use kinds_f90, only: wp
 
contains

  !-----------------------------------------------------------------------------------
  !Output forces per time step
  !-----------------------------------------------------------------------------------
  subroutine output_forces(itr, forces, unit_label)
    implicit none

    !Declarations
    integer,               intent(in) :: itr
    real(wp), allocatable, intent(in) :: forces(:,:)
    character(len=*),      intent(in) :: unit_label
    integer           :: ia
    character(len=20) :: fname
    character(len=5)  :: itr_lab

    !Main Routine
    write(itr_lab,'(I5)') itr
    fname='forces_'//trim(adjustl(itr_lab))//'_'//trim(adjustl(unit_label))//'.dat'
    open(unit=100,file=trim(adjustl(fname)))
    write(100, "(A,I2,A)") 'Outputting total forces for time step, ',itr
    write(100,*) 'In units consistent with '//trim(adjustl(unit_label))
   
    do ia=1,size(forces, dim=2)
       write(100, "(3F20.12)") forces(:, ia)
    end do
    close(100)
    
  end subroutine output_forces

  
  
  !------------------------------------------------------------------------
  ! Convert between internal units of DLPOLY and DFTB+, or vice versa
  ! DFTB+ internal units are 'Hartree atomic units'
  ! DL POLY internal units given on page 7 of the manual
  !------------------------------------------------------------------------
  function convert_unit(from,to,unit) result(factor)
    use constants_api, only: AA__Bohr,Bohr__AA,EMolecular__J,Hartree__J
    implicit none

    !----------------------
    !Declarations
    !----------------------
    character(len=*),  intent(in)  :: from,to
    character(len=*),  intent(in)  :: unit
    real(wp)                       :: factor
    character(len=len(from))       :: from_local
    character(len=len(to))         :: to_local
    character(len=len(unit))       :: unit_local

    !----------------------
    !Main Routine
    !----------------------
    factor=0._wp

    !Enforce case consistency
    from_local= trim(adjustl(uppercase(from)))
    to_local  = trim(adjustl(uppercase(to)))
    unit_local= trim(adjustl(lowercase(unit)))

    !No unit conversion required
    if( from_local == to_local )then
       factor=1._wp
       return
    endif
       
    !DLPOLY to DFTB+
    if(from_local=='DLPOLY' .and. to_local=='DFTB')then
       if(unit_local=='length' )then
          factor=AA__Bohr
          return
       elseif( unit_local=='force' )then
          factor = (EMolecular__J/Hartree__J)*Bohr__AA
          return
       else
          write(*,*)'Not able to convert between internal units for ',unit_local
          write(*,*)'WARNING:conversion factor will be zeroed'
          return
       endif
    endif

    !DFTB+ to DLPOLY
    if(from_local=='DFTB' .and. to_local=='DLPOLY')then  
       if(unit_local=='length' )then
          factor=Bohr__AA
          return
       elseif( unit_local=='force' )then
          factor = (Hartree__J/EMolecular__J)*AA__Bohr
          return
       else
          write(*,*)'Not able to convert between internal units for (',unit_local,')'
          write(*,*)'WARNING:conversion factor will be zeroed'
          return
       endif
    endif

    !Erroneous inputs for to/from
    write(*,*) 'Options not recognised: ',from_local,' and/or ',to_local
    write(*,*) 'WARNING:conversion factor will be zeroed'
  end function convert_unit


  !Get number of lines in file 
  function number_of_lines(fname) result(nlines)
    implicit none
    character(len=*), intent(in) :: fname
    integer                      :: ios,nlines
    nlines = 0
    open(unit=100,file=trim(adjustl(fname)), form='formatted', status='old', iostat=ios)
    
    if(ios/=0)then
       write(*,*) 'Error openning: '//trim(adjustl(fname))
       write(*,*) 'DLPOLY has stopped'
       stop
    endif
    
    do
       read(100,*,end=1000)
       nlines=nlines+1
    enddo
    rewind(unit=100)
    
    1000 close(100)
  end function number_of_lines


  
  !Convert string to lowercase
  !Ref: http://fortranwiki.org/fortran/show/String_Functions
  function lowercase(input)  result (output)
    implicit none
    character(len=*)       :: input
    character(len(input))  :: output
    integer, parameter     :: DUC = ICHAR('A') - ICHAR('a')
    integer                :: i
    character              :: ch    
    do i = 1,len(input)
       ch = input(i:i)
       if (ch >= 'A'.and.ch <= 'Z') ch = char(ichar(ch)-DUC)
       output(i:i) = ch
    enddo
  end function lowercase

  
  !Convert string to uppercase
  function uppercase(input)  result (output)
    character(len=*)       :: input
    character(len(input))  :: output
    integer,parameter      :: duc = ichar('a') - ichar('a')
    character              :: ch
    integer                :: i
    do i = 1,len(input)
       ch = input(i:i)
       if (ch >= 'a'.and.ch <= 'z') ch = char(ichar(ch)+duc)
       output(i:i) = ch
    end do
  end function uppercase
  
  

end module utilities
