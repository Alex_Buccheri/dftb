!**************************************************************************************************
! Test DFTB library. July 2018.
! Alexander Buccheri. CCC UoB. ab17369@bristol.ac.uk 
!
! Note, this is a test of the library, not of the API
! Read in DL CONFIG file and call DLPOLY_DFTB+ interface to do a calculation
!
!**************************************************************************************************

program call_lib
  !DLPOLY's DFTB API
  use dftb_api,       only: w_calculate_dftb_forces
  use utilities,      only: output_forces
  implicit none

  !-----------------
  !Declarations
  !-----------------
  integer, parameter :: dp = selected_real_kind(15, 307)
  !DL Poly CONFIG data
  integer                       :: levcfg, imcon, megatm, itr, nrite
  real(dp),         allocatable :: xxx(:),yyy(:),zzz(:),cell(:),fxx(:),fyy(:),fzz(:)
  character(len=8), allocatable :: atmnam(:)
  Character(Len=8), allocatable :: unqatm(:)

  !Modular variables/arrays/objects
  integer   :: Nsteps
  real(dp), allocatable :: forces(:,:)
  
  !-----------------
  !Main Routine
  !-----------------
  !Dummy values
  nrite=1
  allocate(unqatm(1))
  unqatm(1)='dummy'
  
  !Assign DLPOLY variables (read from file)
  call read_DLPOLY_config(levcfg, imcon, megatm, cell, xxx,yyy,zzz, atmnam)
 
  !Call dftb+ library
  Nsteps=2
  allocate(fxx(megatm),fyy(megatm),fzz(megatm))
  do itr=0,Nsteps-1
     call w_calculate_dftb_forces(itr,nrite, levcfg,imcon,megatm,cell,xxx,yyy,zzz,atmnam,unqatm,&
                                  fxx,fyy,fzz)
  enddo
  
  !Output forces - Done in 'w_calculate_dftb_forces'
  !allocate(forces(3,megatm))
  !forces=0._dp
  !forces(1,:)=fxx(:)
  !forces(2,:)=fyy(:)
  !forces(3,:)=fzz(:)
  !call output_forces(itr, forces, 'DFTB')

  
contains

  !**********************************************************************************
  ! Read all data from DLPOLY config file 
  !**********************************************************************************
  subroutine read_DLPOLY_config(levcfg, imcon, megatm, cell, xxx,yyy,zzz, atmnam)
    implicit none

    !-----------------
    !Declarations
    !----------------
    !Modular variables and arrays
    integer,                       intent(out) :: levcfg, imcon, megatm
    real(dp),         allocatable, intent(out) :: xxx(:),yyy(:),zzz(:),cell(:)
    character(len=8), allocatable, intent(out) :: atmnam(:)

    !Local variables
    integer            :: ierr, dummy_index,ia
    real(dp)           :: vx,vy,vz,fx,fy,fz
    character(len=100) :: header 

    !-----------------
    !Main Routine 
    !----------------
    !See my notes on DL CONFIG file or DL manual 
    !See 'readTGeometryGen_help' in typegeometryhsd.F90

    !Open CONFIG
    open(unit=100,file='CONFIG',status='old',access='sequential',iostat=ierr)  
    if(ierr/=0)then
       write(*,*) 'Can''t find DL POLY CONFIG file.'
       write(*,*) 'read_DLPOLY_vars has stopped'
       stop
    endif

    !Line 1. Header info not used 
    read(100,*) header 

    !Line 2
    read(100,*) levcfg, imcon, megatm

    !imcon relates to the boundary conditions but for now, using
    !finite boundaries with DFTB, regardless of DL input 

    !Lines 3-6. Supercell vectors
    allocate(cell(9))
    read(100,*) cell(1:3)
    read(100,*) cell(4:6)
    read(100,*) cell(7:9)

    !Lines 7 -> 7+(levcfg*megatm) 
    !i.e. the rest of the file
    allocate(atmnam(megatm),xxx(megatm),yyy(megatm),zzz(megatm))

    !Coordinates only
    if(levcfg==0)then
       do ia=1,megatm
          read(100,*) atmnam(ia), dummy_index
          read(100,*) xxx(ia),yyy(ia),zzz(ia)
       enddo
    endif

    !Coordinates and velocities
    if(levcfg==1)then
       write(*,*) 'CONFIG initial velocities not stored'
       do ia=1,megatm
          read(100,*) atmnam(ia), dummy_index
          read(100,*) xxx(ia),yyy(ia),zzz(ia)
          read(100,*) vx,vy,vz
       enddo
    endif

    !Coordinates, velocities and forces
    if(levcfg==2)then
       write(*,*) 'CONFIG initial velocities & forces not stored'
       do ia=1,megatm
          read(100,*) atmnam(ia), dummy_index
          read(100,*) xxx(ia),yyy(ia),zzz(ia)
          read(100,*) vx,vy,vz
          read(100,*) fx,fy,fz
       enddo
    endif

  end subroutine read_DLPOLY_config


  
end program call_lib
