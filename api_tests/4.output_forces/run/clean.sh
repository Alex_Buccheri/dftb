#!/bin/bash

rm band.out \
charges.bin \
detailed.out \
dftb_pin.hsd \
results.tag \
terminal.out \
forces_* \
struct* \
charge* \
*~
