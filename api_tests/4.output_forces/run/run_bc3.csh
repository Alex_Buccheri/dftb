#!/bin/bash
#PBS -V
#PBS -l nodes=1:ppn=16
#PBS -q testq

#Set variables
EXE=$dftbp/api_tests/4.output_forces/build/call.exe 
OUT=terminal.out
NUMPROC=`wc -l < $PBS_NODEFILE`

#Change to submission dir on compute node
cd $PBS_O_WORKDIR
# set the number of OpenMP threads to match the resource request:
export OMP_NUM_THREADS=$NUMPROC
#Run (with timing)
time $EXE > $OUT




