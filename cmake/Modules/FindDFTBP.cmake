#.rst:
# -----------
# FindDFTBP
# -----------
#
# Find the DFTB+ library.
#
# This module defines the following variables:
# DFTB_LIBRARIES     
#

set(_DFTBP_LIBS_SEARCH)
set(_DFTBP_INCDIR_SEARCH)


# Search for environment variable first
if(EXISTS "$ENV{DFTB_ROOT}")
  set(_DFTBP_SEARCH_ROOT PATHS $ENV{DFTB_ROOT}/lib NO_DEFAULT_PATH)
  list(APPEND _DFTBP_LIBS_SEARCH _DFTBP_SEARCH_ROOT)
endif()

#DFTB+ Library 
foreach(search ${_DFTBP_LIBS_SEARCH})
    find_library(DFTBP dftb ${${search}} )
endforeach() 
set( DFTBP_LIBRARY ${DFTBP})


set(_DFTBP_INCDIR_SEARCH PATHS $ENV{DFTB_ROOT}/include NO_DEFAULT_PATH)
# mod files - api uses one of the DFTB types
foreach(search ${_DFTBP_INCDIR_SEARCH})
    find_path (WRAP_INCLUDE_DIR typegeometry.mod ${${search}})
endforeach()
set(DFTBP_INCLUDE_DIRS "${WRAP_INCLUDE_DIR}")


if(NOT ${DFTBP_LIBRARY} EQUAL "")
   set(DFTBP_FOUND TRUE)
else()
  set(DFTBP_FOUND FALSE)
endif()

message("-- DFTBP_LIBRARY location: ${DFTBP_LIBRARY}")




